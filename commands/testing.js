const Discord = require("discord.js")

exports.run = function(bot, msg) {
    bot.embed(msg, bot.hex, null, `Type 1 to go to page 1 or exit to exit!`);
    const collector = new Discord.MessageCollector(msg.channel, m => m.author.id === msg.author.id, { time: 30000 });
    collector.on('collect', message => {
        if (message.content == "1") {
            message.channel.bulkDelete(3, true);
            bot.embed(msg, bot.hex, null, `You have entered Page 1!`);
            bot.embed(msg, bot.hex, null, `Say yes, no, or exit!`);
            collector.on('collect', message => {
                if (message.content == "yes") {
                    message.channel.bulkDelete(3, true);
                    bot.embed(msg, bot.hex, null, `You have said yes!`);
                    collector.stop("Stopped listening after user said exit!")
                } else if (message.content == "no") {
                    message.channel.bulkDelete(3, true);
                    bot.embed(msg, bot.hex, null, `You have said no!`);
                    collector.stop("Stopped listening after user said exit!")
                } else if (message.content == "exit") {
                    message.channel.bulkDelete(3, true);
                    collector.stop("Stopped listening after user said exit!")
                }
            })
        } else if (message.content == "exit") {
            message.channel.bulkDelete(3, true);
            collector.stop("Stopped listening after user said exit!")
        }
    })
};

exports.conf = {
  activated: true,
  aliases: [],
  permLevel: 10
};
  
exports.help = {
  name: 'testing',
  description: 'idek tbh just testing stuff',
  usage: 'testing'
};