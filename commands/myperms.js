exports.run = function(bot, msg) {
  const selfPermission = bot.permission(msg);
  bot.embed(msg, bot.hex, "Fetched your permission level:", `Your permission level is: \`${selfPermission}\``); 
}

exports.conf = {
    activated: true,
    aliases: [],
    permLevel: 0
  };
  
  exports.help = {
    name: 'myperms',
    description: 'Get my permissions.',
    usage: 'myperms'
  };